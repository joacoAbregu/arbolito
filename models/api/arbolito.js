const mongoose = require('mongoose');

const arbolSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    unique: true,
    default: '',
  },
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Usuario',
  },
  avatar: {
    type: String,
    default: '',
  },
  links: {
    type: Array,
    default: [
      {
        name: '',
        url: '',
      },
      {
        name: '',
        url: '',
      },
      {
        name: '',
        url: '',
      },
      {
        name: '',
        url: '',
      },
      {
        name: '',
        url: '',
      },
    ],
  },
  facebook: {
    type: String,
    default: '',
  },
  twitter: {
    type: String,
    default: '',
  },
  instagram: {
    type: String,
    default: '',
  },
  youtube: {
    type: String,
    default: '',
  },
});

arbolSchema.statics.add = function add(arbolito, cb) {
  this.create(arbolito, cb);
};

arbolSchema.statics.findUser = function findUser(user, cb) {
  this.findOne({ name: user }, cb);
};
arbolSchema.statics.findId = function findId(id, cb) {
  this.findOne({ _userId: id }, cb);
};
module.exports = new mongoose.model('Arbolito', arbolSchema);
