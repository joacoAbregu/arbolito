const mongoose = require('mongoose');
const crypto = require('crypto');
const bcrypt = require('bcrypt');

const saltRounds = 10;
const Token = require('./token');
const mailer = require('../mailer/mailer');
const Arbolito = require('./api/arbolito');

const validateEmail = (email) => {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const usuarioSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    unique: true,
  },
  email: {
    type: String,
    required: [true, 'El email es obligatorio'],
    lowercase: true,
    trim: true,
    unique: true,
    validate: [validateEmail, 'El email no es valido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/],
  },
  password: {
    type: String,
    trim: true,
    required: [true, 'El password es obligatorio'],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false,
  },
});

usuarioSchema.pre('save', function (next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function validatePass(password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.enviar_email_bienvenida = function enviarEmail(cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString('hex'),
  });
  const emailDestination = this.email;

  token.save(function (err) {
    if (err) {
      console.log(err);
    }
    const mailOptions = {
      from: process.env.MAIL_SENDER,
      to: emailDestination,
      subject: 'Verificación de cuenta',
      text: 'Hola,\n\n' + 'Por favor, para verificar tu cuenta hacé click en el siguiente link:\n\n' + process.env.BASE_URL + '\/token/confirmation\/' + token.token + ' .\n' + 'Luego, ingresá con tus datos.',
      html: `<h1>Hola</h1> <p>Por favor, para verificar tu cuenta hacé click en el siguiente: <a href="${process.env.BASE_URL}\/token/confirmation\/${token.token}">link</a></p> <p>Luego, ingresá con tus datos.</p>`,
    };

    mailer.sendMail(mailOptions, (error) => {
      if (error) {
        console.log(error);
      }
    });
  });
};

usuarioSchema.methods.resetPassword = function resetPassword(cb) {
  const token = new Token({
    _userId: this.id,
    token: crypto.randomBytes(16).toString('hex'),
  });
  const emailDestination = this.email;

  token.save((err) => {
    if (err) {
      return cb(err);
    }

    const mailOptions = {
      from: process.env.MAIL_SENDER,
      to: emailDestination,
      subject: 'Reseteo de Password de Cuenta',
      text: 'Hola,\n\n' + 'Por favor, para resetear la contraseña de tu cuenta hace click en este link:\n\n' +
      process.env.BASE_URL + '\/password/reset\/' + token.token,
      html: `<h1>Hola</h1> <p>Por favor, para resetear la contraseña de tu cuenta hace click en este <a href="${process.env.BASE_URL}\/password/reset\/${token.token}">link</a></p>`,
    };

    mailer.sendMail(mailOptions, (error) => {
      if (error) {
        console.log(error);
        return cb(error);
      }
    });
    cb(null);
  });
};

usuarioSchema.methods.crearArbolito = async function (id, name) {
  const miArbolito = new Arbolito({
    _userId: id,
    name,
  });
  await Arbolito.create(miArbolito);
};

module.exports = new mongoose.model('Usuario', usuarioSchema);
