const { body, validationResult } = require('express-validator');
const Arbolito = require('../models/api/arbolito');

const registerValidationRules = () => [
  body('name')
    .isLength({ min: 2 })
    .withMessage('El nombre debe tener al menos 2 caracteres')
    .escape(),
  body('email')
    .isEmail()
    .normalizeEmail(),
  body('password')
    .isLength({ min: 5 })
    .withMessage('La contraseña debe tener al menos 5 caracteres'),
];

const passwordValidationRules = () => [
  body('email')
    .isEmail()
    .normalizeEmail(),
];

const panelValidationRules = () => [
  body('name')
    .escape(),
  body('nameOne')
    .escape(),
  body('UrlOne')
    .escape(),
  body('nameTwo')
    .escape(),
  body('UrlTwo')
    .escape(),
  body('nameThree')
    .escape(),
  body('UrlThree')
    .escape(),
  body('nameFour')
    .escape(),
  body('UrlFour')
    .escape(),
  body('nameFive')
    .escape(),
  body('UrlFive')
    .escape(),
  body('facebook')
    .escape(),
  body('twitter')
    .escape(),
  body('instagram')
    .escape(),
  body('youtube')
    .escape(),
];

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));
  return res.status(422).json({
    errors: extractedErrors,
  });
};

const panelValidate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));
  const firstError = extractedErrors[0];
  const message = firstError[Object.keys(firstError)[0]];
  Arbolito.findId(req.params.id, function (err, user) {
    if (err) {
      const messageError = 'Ha ocurrido un error. Volvé a intentarlo';
      res.status(400);
      return res.render('error', { messageError, error: {}, info: {} });
    }
    if (user === undefined) {
      const messageUser = 'Usuario no encontrado. Volvé a intentarlo';
      res.status(403);
      return res.render('error', { messageUser, error: {}, info: {} });
    }
    res.status(422);
    return res.render('../views/panel/panelUpdate', { user, error: { message }, csrfToken: req.csrfToken() });
  });
};

const registerValidate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  errors.array().map((err) => extractedErrors.push({ [err.param]: err.msg }));
  res.status(422);
  return res.render('register', {
    info: {},
    error: { message: extractedErrors },
    csrfToken: req.csrfToken(),
  });
};

module.exports = {
  registerValidationRules,
  passwordValidationRules,
  panelValidationRules,
  registerValidate,
  panelValidate,
  validate,
};
