require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const helmet = require('helmet');
const compression = require('compression');
const rateLimit = require('express-rate-limit');

// Routers
const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');
const panelRouter = require('./routes/panel');
const registerRouter = require('./routes/register');
const tokenRouter = require('./routes/token');
const passwordRouter = require('./routes/password');

const passport = require('./config/passport');

const csrfProtection = csrf();
const mongoDB = process.env.MONGO_URI;
let store;

// LoggedIn middleware
function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    res.redirect('/');
  }
}
// Express rate limiter
// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
// see https://expressjs.com/en/guide/behind-proxies.html
// app.set('trust proxy', 1);
const createAccountLimiter = rateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour window
  max: 5,
  handler: function (req, res) {
    const message = 'Creaste demasiadas cuentas. Por favor volvé a intentar más tarde';
    return res.status(429).render('error', { message, error: {}, info: {} });
  },
});

const loginLimiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 15 minutes
  max: 15,
  handler: function (req, res) {
    const message = 'Intentaste acceder demasiadas veces. Por favor volvé a intentarlo más tarde';
    return res.status(429).render('error', { message, error: {}, info: {} });
  },
});

if (process.env.NODE_ENV === 'development') {
  store = session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions',
  });

  store.on('error', (err) => {
    assert.ifError(err);
    assert.ok(false);
  });
}

const app = express();
app.use(helmet());
app.use(compression());
// Session
app.use(session({
  cookie: {
    maxAge: 240 * 60 * 60 * 1000,
  },
  store,
  saveUninitialized: true,
  resave: 'true',
  secret: process.env.SESSION_SECRET,
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', loginLimiter, csrfProtection, indexRouter);
app.use('/user', userRouter);
app.use('/panel', loggedIn, csrfProtection, panelRouter);
app.use('/register', createAccountLimiter, csrfProtection, registerRouter);
app.use('/token', tokenRouter);
app.use('/password', csrfProtection, passwordRouter);

mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to server'));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
