const Arbolito = require('../models/api/arbolito');

exports.user_get = (req, res) => {
  Arbolito.findUser(req.params.user, (err, user) => {
    if (user === null || err) {
      const message = 'Se produjo un error. Usuario no encontrado';
      return res.render('error', { message, error: {}, info: {} });
    }
    return res.render('user', {
      user, error: {}, info: {}, csrfToken: req.csrfToken(),
    });
  });
};
