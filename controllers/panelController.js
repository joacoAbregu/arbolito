const fs = require('fs');
const tinify = require('tinify');
const path = require('path');
const Arbolito = require('../models/api/arbolito');

tinify.key = process.env.TINIFY;

exports.panel_get = function (req, res) {
  Arbolito.findId(req.params.id, function (err, user) {
    if (err) {
      const message = 'Ha ocurrido un error. Volvé a intentarlo';
      res.status(400);
      return res.render('error', { message, error: {}, info: {} });
    }
    if (user === undefined) {
      const message = 'Usuario no encontrado';
      res.status(403);
      return res.render('error', { message, error: {}, info: {} });
    }
    const userRequest = req.user._id;
    const userPedido = userRequest.toString();
    const userArbol = user._userId;
    const arbolPedido = userArbol.toString();
    if (userPedido === arbolPedido) {
      return res.render('../views/panel/panelUpdate', { user, error: {}, csrfToken: req.csrfToken() });
    }
    const message = 'No tenés permiso para acceder a esta página';
    res.status(403);
    return res.render('error', { message, error: {}, info: {} });
  });
};

exports.panel_post = async function (req, res, next) {
  let avatar = '';
  function imageStoring(buffer, imgPath) {
    return new Promise(function (resolve, reject) {
      tinify.fromBuffer(buffer).toBuffer(function (err, resultData) {
        if (err) reject(err);
        fs.writeFile(imgPath, resultData, function (error) {
          if (error) reject(error);
          resolve('Results written');
        });
      });
    });
  }

  const nombre = req.body.name;
  const {
    facebook, instagram, twitter, youtube,
  } = req.body;
  const {
    nameOne, nameTwo, nameThree, nameFour, nameFive,
  } = req.body;
  const {
    urlOne, urlTwo, urlThree, urlFour, urlFive,
  } = req.body;

  if (req.file !== undefined) {
    try {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
      const avatarName = req.file.fieldname + '-' + uniqueSuffix + path.extname(req.file.originalname);
      const imagePath = './public/images/' + avatarName;
      avatar = '/images/' + avatarName;
      const miArbolito = new Arbolito({
        name: nombre,
        avatar,
        facebook,
        instagram,
        twitter,
        youtube,
        links: [
          { name: nameOne, url: urlOne },
          { name: nameTwo, url: urlTwo },
          { name: nameThree, url: urlThree },
          { name: nameFour, url: urlFour },
          { name: nameFive, url: urlFive }],
      });
      imageStoring(req.file.buffer, imagePath)
        .then((result) => {
          Arbolito.add(miArbolito, function (err, nuevoArbolito) {
            res.redirect(`/panel/${nuevoArbolito._id}`);
          });
        })
        .catch((err) => next(err));
    } catch (error) {
      next(error);
    }
  } else {
    const miArbolito = new Arbolito({
      name: nombre,
      avatar,
      facebook,
      instagram,
      twitter,
      youtube,
      links: [{ name: nameOne, url: urlOne }, { name: nameTwo, url: urlTwo }, { name: nameThree, url: urlThree }, { name: nameFour, url: urlFour }, { name: nameFive, url: urlFive }],
    });
    Arbolito.add(miArbolito, function (err, nuevoArbolito) {
      if (err) {
        const message = 'Ha ocurrido un error. Volvé a intentarlo';
        res.status(400);
        return res.render('error', { message, error: {}, info: {} });
      }
      res.redirect(`/panel/${nuevoArbolito._id}`);
    });
  }
};

exports.panel_update = function (req, res, next) {
  Arbolito.findId(req.params.id, function (err, user) {
    if (err) {
      const message = 'Ha ocurrido un error. Volvé a intentarlo';
      res.status(400);
      return res.render('error', { message, error: {}, info: {} });
    }
    if (req.file !== undefined) {
      // Delete old avatar photo
      const oldAvatarPath = './public' + user.avatar;
      fs.unlink(oldAvatarPath, (error) => {
        if (error) {
          console.log('Something wrong happened removing the file');
        }
      });
      // Update user and avatar
      try {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        const avatarName = req.file.fieldname + '-' + uniqueSuffix + path.extname(req.file.originalname);
        const imagePath = './public/images/' + avatarName;
        const avatar = '/images/' + avatarName;
        user.avatar = avatar;
        // Compress image with tinify
        tinify.fromBuffer(req.file.buffer).toBuffer(function (error, resultData) {
          if (error) return next(error);
          fs.writeFile(imagePath, resultData, function (errFs) {
            if (errFs) next(errFs);
            user.nombre = req.body.name;
            const {
              facebook, instagram, twitter, youtube,
            } = req.body;
            const {
              nameOne, nameTwo, nameThree, nameFour, nameFive,
            } = req.body;
            const {
              urlOne, urlTwo, urlThree, urlFour, urlFive,
            } = req.body;
            user.facebook = facebook;
            user.instagram = instagram;
            user.twitter = twitter;
            user.youtube = youtube;
            user.links = [{ name: nameOne, url: urlOne }, { name: nameTwo, url: urlTwo }, { name: nameThree, url: urlThree }, { name: nameFour, url: urlFour }, { name: nameFive, url: urlFive }];
            user.save()
              .then((user) => {
              // If everything goes as planed
              // use the retured user document for redirect
                res.redirect(`/panel/${user._userId}`);
              })
              .catch((err) => {
              // When there are errors We handle them here
                let message = '';
                if (err.code === 11000) {
                  message = `${err.keyValue[Object.keys(err.keyValue)[0]]} ya está en uso. Elegí otro.`;
                }
                return res.render('../views/panel/panelUpdate', { user, error: { message }, csrfToken: req.csrfToken() });
              });
          });
        });
      } catch (error) {
        next(error);
      }
    } else {
    // Actualizar usuario sin avatar
      user.name = req.body.name;
      const {
        facebook, instagram, twitter, youtube,
      } = req.body;
      const {
        nameOne, nameTwo, nameThree, nameFour, nameFive,
      } = req.body;
      const {
        urlOne, urlTwo, urlThree, urlFour, urlFive,
      } = req.body;
      user.facebook = facebook;
      user.instagram = instagram;
      user.twitter = twitter;
      user.youtube = youtube;
      user.links = [{ name: nameOne, url: urlOne }, { name: nameTwo, url: urlTwo }, { name: nameThree, url: urlThree }, { name: nameFour, url: urlFour }, { name: nameFive, url: urlFive }];
      user.save()
        .then((user) => {
          res.redirect(`/panel/${user._userId}`);
        })
        .catch((err) => {
          // Si el nombre ya está tomado en la base de datos enviar el siguiente mensaje de error
          let message = '';
          if (err.code === 11000) {
            message = `${err.keyValue[Object.keys(err.keyValue)[0]]} ya está en uso. Elegí otro.`;
          }
          res.render('../views/panel/panelUpdate', { user, error: { message }, csrfToken: req.csrfToken() });
        });
    }
  });
};
