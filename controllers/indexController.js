const passport = require('../config/passport');

exports.index_get = function (req, res) {
  res.render('index', { info: {}, csrfToken: req.csrfToken() });
};

exports.index_login = function (req, res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if (err) {
      return next(err);
    }
    if (!usuario) {
      return res.render('index', { info, csrfToken: req.csrfToken() });
    }
    req.logIn(usuario, function (error) {
      if (error) {
        return next(error);
      }
      return res.redirect(`/panel/${usuario._id}`);
    });
  })(req, res, next);
};

exports.logout = function (req, res) {
  req.logOut();
  if (req.session) {
    req.session.destroy();
  }
  res.redirect('/');
};
