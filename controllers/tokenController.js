const Usuario = require('../models/usuario');
const Token = require('../models/token');

module.exports = {
  confirmation_get: function (req, res) {
    Token.findOne({ token: req.params.token }, function (err, token) {
      if (err) {
        const message = 'Ha ocurrido un error. Volvé a intentarlo';
        res.status(400);
        return res.render('error', { message, error: {}, info: {} });
      }
      if (!token) {
        return res.status(400).send({
          type: 'not-verified',
          msg: 'No se encontró usuario con ese token. Quizas haya espirado y debas solicitar uno nuevo',
        });
      }
      Usuario.findById(token._userId, function (error, usuario) {
        if (error) {
          const message = 'Ha ocurrido un error. Volvé a intentarlo';
          res.status(400);
          return res.render('error', { message, error: {}, info: {} });
        }
        if (!usuario) return res.status(400).send({ msg: 'No encontramos un usuario con este token' });
        if (usuario.verificado) return res.redirect('/');
        usuario.verificado = true;
        usuario.save(function (errSave) {
          if (errSave) { return res.status(500).send({ msg: errSave.message }); }
          res.redirect('/');
        });
      });
    });
  },
};
