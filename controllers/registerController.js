const Usuario = require('../models/usuario');

exports.register_get = (req, res) => {
  res.render('register', { error: {}, info: {}, csrfToken: req.csrfToken() });
};

exports.register_post = (req, res) => {
  Usuario.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
  }, function (err, nuevoUsuario) {
    if (err) {
      console.log(err);
      let message = 'Se ha producido un error durante el registro. Si continúa contactanos por mail.';
      if (err.errors) {
        message = err.errors.password.message;
      }
      if (err.code === 11000) {
        message = `${err.keyValue[Object.keys(err.keyValue)[0]]} ya está en uso. Elegí otro.`;
      }
      return res.render('register', {
        info: {},
        error: { message },
        csrfToken: req.csrfToken(),
      });
    }
    nuevoUsuario.crearArbolito(nuevoUsuario._id, nuevoUsuario.name);
    nuevoUsuario.enviar_email_bienvenida();
    res.render('registerMessage');
  });
};
