const Usuario = require('../models/usuario');
const Token = require('../models/token');

exports.forgot_get = function (req, res) {
  res.render('password/passwordForgot', { info: {}, csrfToken: req.csrfToken() });
};

exports.forgot_post = function (req, res, next) {
  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (!usuario) {
      return res.render('password/passwordForgot', {
        info: { message: 'No existe un usuario con el email ingresado' },
        csrfToken: req.csrfToken()
      });
    }
    usuario.resetPassword(function (error) {
      if (error) {
        return next(error);
      }
    });
    res.render('password/passwordMessage');
  });
};

exports.reset_get = function (req, res) {
  Token.findOne({ token: req.params.token }, function (err, token) {
    if (err) {
      const message = 'Ha ocurrido un error. Volvé a intentarlo';
      res.status(400);
      return res.render('error', { message, error: {}, info: {} });
    }
    if (!token) {
      const message = 'No existe un usuario asociado al token. Verificá que tu token no haya expirado.';
      res.status(400);
      return res.render('error', { message, error: {}, info: {} });
    }
    Usuario.findById(token._userId, function (err, usuario) {
      if (err) {
        const message = 'Token equivocado. Asegurese de haber ingresado el token completo';
        res.status(400);
        return res.render('error', { message, error: {}, info: {} });
      }
      if (!usuario) {
        const message = 'No existe un usuario asociado al token.';
        res.status(400);
        return res.render('error', { message, error: {}, info: {} });
      }

      res.render('password/passwordReset', { errors: {}, usuario, info: {}, csrfToken: req.csrfToken() });
    });
  });
};

exports.reset_post = function (req, res) {
  if (req.body.password !== req.body.confirm_password) {
    res.render('password/passwordReset', {
      errors: {
        confirm_password: {
          message: 'No coincide el password ingresado',
        },
      },
      usuario: new Usuario({ email: req.body.email }),
      csrfToken: req.csrfToken(),
      info: {},
    });
    return;
  }

  Usuario.findOne({ email: req.body.email }, function (err, usuario) {
    if (err) {
      const message = 'Ha ocurrido un error. Volvé a intentarlo';
      res.status(400);
      return res.render('error', { message, error: {}, info: {} });
    }
    usuario.password = req.body.password;
    usuario.save(function (error) {
      if (error) {
        return res.render('password/passwordReset', {
          errors: error.errors,
          usuario: new Usuario({ email: req.body.email }),
          csrfToken: req.csrfToken()
        });
      } else {
        res.redirect('/');
      }
    });
  });
};
