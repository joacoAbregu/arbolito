const express = require('express');

const router = express.Router();
const userController = require('../controllers/userController');

/* GET user page. */
router.get('/:user', userController.user_get);

module.exports = router;
