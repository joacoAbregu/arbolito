const express = require('express');

const router = express.Router();
const indexController = require('../controllers/indexController');

/* GET home page. */
router.get('/', indexController.index_get);
/* POST Login */
router.post('/', indexController.index_login);
/* Logout */
router.get('/logout', indexController.logout);

module.exports = router;
