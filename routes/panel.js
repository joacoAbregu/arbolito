const express = require('express');
const multer = require('multer');
const panelController = require('../controllers/panelController');
const { panelValidationRules, panelValidate } = require('../validators/validators');

const router = express.Router();
const storage = multer.memoryStorage();
const upload = multer({ storage });

/* GET panel page. */

router.get('/:id', panelController.panel_get);
router.post('/', upload.single('avatar'), panelController.panel_post);
router.post('/:id', panelValidationRules(), panelValidate, upload.single('avatar'), panelController.panel_update);

module.exports = router;
