const express = require('express');
const { registerValidationRules, registerValidate } = require('../validators/validators');

const router = express.Router();
const registerController = require('../controllers/registerController');

/* GET register page. */
router.get('/', registerController.register_get);
/* POST register page. */
router.post('/', registerValidationRules(), registerValidate, registerController.register_post);

module.exports = router;
