const express = require('express');
const { passwordValidationRules, validate } = require('../validators/validators');

const router = express.Router();
const passwordController = require('../controllers/passwordController');

/* GET forgot password */
router.get('/forgot', passwordController.forgot_get);
/* POST forgot password  */
router.post('/forgot', passwordValidationRules(), validate, passwordController.forgot_post);
/* GET reset password */
router.get('/reset/:token', passwordController.reset_get);
/* POST reset password  */
router.post('/reset', passwordController.reset_post);

module.exports = router;
