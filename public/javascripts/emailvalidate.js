const email = document.getElementById('email');
const form = document.getElementById('form');
// const emailAlert = document.getElementById("log");
const btnEmail = document.getElementById('form__btn');
function emailIsValid(mail) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(mail);
}

form.addEventListener('submit', function (e) {
  e.preventDefault();
  if (!emailIsValid(email.value)) {
    log.textContent = 'Por favor ingresar un mail válido';
    e.preventDefault();
  } else {
    btnEmail.textContent = 'Registrando';
    setTimeout(function () {
      form.submit();
    }, 800);
  }
});
